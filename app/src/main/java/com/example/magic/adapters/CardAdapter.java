package com.example.magic.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.magic.R;
import com.example.magic.models.Card;

import java.util.ArrayList;
import java.util.List;

public class CardAdapter extends ArrayAdapter<Card> {
    public CardAdapter(Context context, int resource, List<Card> cards) {
        super(context, resource, cards);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        Card card = getItem(position);
        System.out.println("XXX" + card.getImageUrl());

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.layout_card, parent, false);
        }

        TextView title = convertView.findViewById(R.id.title);
        TextView description = convertView.findViewById(R.id.description);
        ImageView image = convertView.findViewById(R.id.imagePoster);
        System.out.println(card.getImageUrl());

        title.setText(card.getName());
        description.setText(card.getText());
        Glide.with(getContext()).load(
                card.getImageUrl()
        ).apply(new RequestOptions().override(600, 200)).into(image);


        return convertView;

    }
}
