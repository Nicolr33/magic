package com.example.magic.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.magic.models.Card;

import java.util.List;

@Dao
public interface CardDao {
    @Query("select * from card")
    LiveData<List<Card>> getCards();

    @Insert
    void addCard(Card card);

    @Insert
    void addCards(List<Card> cards);

    @Delete
    void deleteCard(Card card);

    @Query("DELETE FROM card")
    void deleteCards();
}
