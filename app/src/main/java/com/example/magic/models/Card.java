package com.example.magic.models;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;
@Entity
public class Card implements Serializable
{
    @PrimaryKey(autoGenerate = true)
    private int idd;

    private String multiverseid;

    private String originalType;

    private String artist;

    private String type;

    private String number;

    private String imageUrl;

    private String text;

    private String id;

    private String set;

    private String layout;

    private String originalText;

    private String name;

    private String cmc;

    private String manaCost;

    private String rarity;

    public String getMultiverseid ()
    {
        return multiverseid;
    }

    public void setMultiverseid (String multiverseid)
    {
        this.multiverseid = multiverseid;
    }

    public int getIdd ()
    {
        return idd;
    }

    public void setIdd (int idd)
    {
        this.idd = idd;
    }

    public String getOriginalType ()
    {
        return originalType;
    }

    public void setOriginalType (String originalType)
    {
        this.originalType = originalType;
    }

    public String getArtist ()
    {
        return artist;
    }

    public void setArtist (String artist)
    {
        this.artist = artist;
    }

    public String getType ()
    {
        return type;
    }

    public void setType (String type)
    {
        this.type = type;
    }

    public String getNumber ()
    {
        return number;
    }

    public void setNumber (String number)
    {
        this.number = number;
    }

    public String getImageUrl ()
    {
        return imageUrl;
    }

    public void setImageUrl (String imageUrl)
    {
        this.imageUrl = imageUrl;
    }

    public String getText ()
    {
        return text;
    }

    public void setText (String text)
    {
        this.text = text;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getSet ()
    {
        return set;
    }

    public void setSet (String set)
    {
        this.set = set;
    }

    public String getLayout ()
    {
        return layout;
    }

    public void setLayout (String layout)
    {
        this.layout = layout;
    }

    public String getOriginalText ()
    {
        return originalText;
    }

    public void setOriginalText (String originalText)
    {
        this.originalText = originalText;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getCmc ()
    {
        return cmc;
    }

    public void setCmc (String cmc)
    {
        this.cmc = cmc;
    }

    public String getManaCost ()
    {
        return manaCost;
    }

    public void setManaCost (String manaCost)
    {
        this.manaCost = manaCost;
    }

    public String getRarity ()
    {
        return rarity;
    }

    public void setRarity (String rarity)
    {
        this.rarity = rarity;
    }
}

