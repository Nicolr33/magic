package com.example.magic.models;

public class ForeignNames
{
    private String flavor;

    private String multiverseid;

    private String imageUrl;

    private String name;

    private String language;

    private String text;

    public String getFlavor ()
{
    return flavor;
}

    public void setFlavor (String flavor)
    {
        this.flavor = flavor;
    }

    public String getMultiverseid ()
    {
        return multiverseid;
    }

    public void setMultiverseid (String multiverseid)
    {
        this.multiverseid = multiverseid;
    }

    public String getImageUrl ()
    {
        return imageUrl;
    }

    public void setImageUrl (String imageUrl)
    {
        this.imageUrl = imageUrl;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getLanguage ()
    {
        return language;
    }

    public void setLanguage (String language)
    {
        this.language = language;
    }

    public String getText ()
    {
        return text;
    }

    public void setText (String text)
    {
        this.text = text;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [flavor = "+flavor+", multiverseid = "+multiverseid+", imageUrl = "+imageUrl+", name = "+name+", language = "+language+", text = "+text+"]";
    }
}