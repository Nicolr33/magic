package com.example.magic.models;

public class Legalities
{
    private String legality;

    private String format;

    public String getLegality ()
    {
        return legality;
    }

    public void setLegality (String legality)
    {
        this.legality = legality;
    }

    public String getFormat ()
    {
        return format;
    }

    public void setFormat (String format)
    {
        this.format = format;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [legality = "+legality+", format = "+format+"]";
    }
}

