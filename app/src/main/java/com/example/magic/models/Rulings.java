package com.example.magic.models;

public class Rulings
{
    private String date;

    private String text;

    public String getDate ()
    {
        return date;
    }

    public void setDate (String date)
    {
        this.date = date;
    }

    public String getText ()
    {
        return text;
    }

    public void setText (String text)
    {
        this.text = text;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [date = "+date+", text = "+text+"]";
    }
}

