package com.example.magic.services;

import android.net.Uri;

import com.example.magic.models.Card;
import com.example.magic.utils.HttpUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public class CardService {
    private String BASE_URL = "https://api.magicthegathering.io/v1/";


    public ArrayList<Card> getCards() {
        Uri uri = Uri.parse(BASE_URL)
                .buildUpon()
                .appendPath("cards")
                .build();

        String url = uri.toString();

        return getJSon(url);
    }


    private ArrayList<Card> getJSon(String url) {
        try {
            String JsonResponse = HttpUtils.get(url);
            return processJson(JsonResponse);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private ArrayList<Card> processJson(String jsonResponse) {
        ArrayList<Card> cards = new ArrayList<>();
        try {
            JSONObject data = new JSONObject(jsonResponse);
            JSONArray jsonMovies = data.getJSONArray("cards");
            for (int i = 0; i < jsonMovies.length(); i++) {
                JSONObject jsonMovie = jsonMovies.getJSONObject(i);

                Card card = new Card();
                card.setName(jsonMovie.getString("name"));
                card.setType(jsonMovie.getString("type"));
                card.setRarity(jsonMovie.getString("rarity"));

                if (jsonMovie.has("imageUrl"))
                    card.setImageUrl(jsonMovie.getString("imageUrl"));

                if (jsonMovie.has("text"))
                    card.setText(jsonMovie.getString("text"));

                cards.add(card);


            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return cards;
    }
}
