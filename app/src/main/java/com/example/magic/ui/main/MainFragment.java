package com.example.magic.ui.main;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.example.magic.R;
import com.example.magic.adapters.CardAdapter;
import com.example.magic.databinding.MainFragmentBinding;
import com.example.magic.models.Card;
import com.example.magic.services.CardService;
import com.example.magic.ui.main.details.Details;
import com.example.magic.ui.main.details.DetailsFragment;

import java.util.ArrayList;

public class MainFragment extends Fragment {

    private MainViewModel mViewModel;
    private ArrayList<Card> items;
    private CardAdapter adapter;
    private ProgressBar spinner;
    private MainFragmentBinding binding;
    private MainViewModel model;
    private ProgressDialog dialog;

    public static MainFragment newInstance() {
        return new MainFragment();
    }


    public void onStart() {
        super.onStart();
        refresh();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = MainFragmentBinding.inflate(inflater);
        View view = binding.getRoot();
        setHasOptionsMenu(true);

        items = new ArrayList<>();
        adapter = new CardAdapter(getContext(), R.layout.layout_card, items);

        binding.lvCards.setAdapter(adapter);
        binding.lvCards.setOnItemClickListener((adapterView, view1, i, l) -> {
            Card card = (Card) adapterView.getItemAtPosition(i);
            Intent intent = new Intent(getContext(), Details.class);
            intent.putExtra("card", card);
            startActivity(intent);
        });

        model = ViewModelProviders.of(this).get(MainViewModel.class);
        model.getCards().observe(this, cards -> {
            adapter.clear();
            adapter.addAll(cards);
        });

        dialog = new ProgressDialog(getContext());
        dialog.setMessage("Loading...");

        model.getLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean mostrat) {
                if (mostrat)
                    dialog.show();
                else
                    dialog.dismiss();
            }
        });


        return view;
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.refresh) {
            refresh();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void refresh() {
        model.reload();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        // TODO: Use the ViewModel
    }

    private class RefreshDataTask extends AsyncTask<Void, Void, ArrayList<Card>> {
        @Override
        protected ArrayList<Card> doInBackground(Void... voids) {
            CardService cardApi = new CardService();
            return cardApi.getCards();
        }

        @Override
        protected void onPostExecute(ArrayList<Card> cards) {
            adapter.clear();
            adapter.addAll(cards);
        }
    }

}
