package com.example.magic.ui.main;

import android.app.Application;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.magic.dao.CardDao;
import com.example.magic.database.AppDatabase;
import com.example.magic.models.Card;
import com.example.magic.services.CardService;

import java.util.ArrayList;
import java.util.List;

public class MainViewModel extends AndroidViewModel {
    private final Application app;
    private final AppDatabase appDatabase;
    private final CardDao cardDao;
    private LiveData<List<Card>> cards;
    private MutableLiveData<Boolean> loading = new MutableLiveData<Boolean>();

    public MainViewModel(@NonNull Application application) {
        super(application);
        this.app = application;
        this.appDatabase = AppDatabase.getDatabase(
                this.getApplication()
        );
        this.cardDao = appDatabase.getCardDao();
    }

    public LiveData<List<Card>> getCards() {
        return cardDao.getCards();
    }

    public void reload() {

        RefreshDataTask task = new RefreshDataTask();
        task.execute();
    }

    public MutableLiveData<Boolean> getLoading() {
        if(loading == null){
            loading = new MutableLiveData<>();
        }
        return loading;
    }


    private class RefreshDataTask extends AsyncTask<Void, Void, ArrayList<Card>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loading.setValue(true);
        }

        @Override
        protected ArrayList<Card> doInBackground(Void... voids) {


            CardService api = new CardService();
            ArrayList<Card> result;

            result = api.getCards();

            cardDao.deleteCards();
            cardDao.addCards(result);

            return result;
        }

        @Override
        protected void onPostExecute(ArrayList<Card> cards) {
            super.onPostExecute(cards);
            loading.setValue(false);
        }


    }


}
