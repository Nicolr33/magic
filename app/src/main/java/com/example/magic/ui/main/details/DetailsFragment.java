package com.example.magic.ui.main.details;

import com.example.magic.databinding.FragmentDetailsBinding;

import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.example.magic.R;
import com.example.magic.models.Card;

/**
 * A placeholder fragment containing a simple view.
 */
public class DetailsFragment extends Fragment {
    private FragmentDetailsBinding binding;

    public DetailsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentDetailsBinding.inflate(inflater);

        Intent i = getActivity().getIntent();

        if (i != null) {
            Card card = (Card) i.getSerializableExtra("card");

            if (card != null) {
                updateUi(card);
            }
        }

        return binding.getRoot();
    }

    private void updateUi(Card card) {
        Log.d("CARD", card.toString());

        binding.detailTitle.setText(card.getName());
        binding.detailArtist.setText(card.getArtist());
        binding.detailDescription.setText(card.getText());
        Glide.with(getContext()).load(
                card.getImageUrl()
        ).into(binding.detailPosterImage);
    }
}
